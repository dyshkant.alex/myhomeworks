import React from "react";
import ReactDOM from "react-dom";

import Button from "../buttons/Button";
import styled from "styled-components";

const Modal = ({ title, isOpen, closeModalHandler, text, isCloseBtn, actions }) => {


  if (isOpen) {
    return ReactDOM.createPortal(
      <div
        onClick={(e) =>
          e.currentTarget === e.target ? closeModalHandler() : null
        }
        className="modal"
      >
        <div className="modal__container">
          <div className="modal__header">
            <h2>{title}</h2>
            {isCloseBtn && (
              <CloseBtn
                className="modal__close-btn"
                onClick={closeModalHandler}
                text="&times;"
                backgroundColor="#b3382c;"
              />
            )}
          </div>
          <div className="modal__text">{text}</div>
          <div className="modal__actions"> {actions.map((it, index)=>{
            
            return it})}</div>
        </div>
        
      </div>,
      document.getElementById("modal-root")
    );
  } else return null;
};

const CloseBtn = styled(Button)`
  font-family: Helvetica, Arial, sans-serif;
  font-size: 15px;
  line-height: 30px;
  color: #ffffff;
  padding: 20px;
  background: ${(props) => props.backgroundColor};
`;

export default Modal;
