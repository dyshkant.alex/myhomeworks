import React, { useState } from "react";
import styled from "styled-components";

import "./App.sass";
import Modal from "./components/modals/Modal";
import Button from "./components/buttons/Button";

function App() {
  const [isOpenFirstModal, setIsOpenFirstModal] = useState(false);
  const [isOpenSecondModal, setIsOpenSecondModal] = useState(false);
  const text = 'Текст в первом модальном окне'
  const text2 = 'Текст во втором модальном окне'
const actions = [<Button  text='submit'/>, <Button text='cancel'/>]
const actions2 =[<Button text='действие'/>, <Button text='отмена '/>] 
  
  return (
    <div className="App">
      <div className="btn-container">
        <StyledButton
          onClick={() => setIsOpenFirstModal(true)}
          backgroundColor="#F4A460"
          className="btn"
          text="open first modal"
        />
        <StyledButton
          onClick={() => setIsOpenSecondModal(true)}
          backgroundColor="#1E90FF"
          className="btn"
          text="open second modal"
        />
      </div>
      <Modal
        isOpen={isOpenFirstModal}
        className="modal"
        closeModalHandler={() => setIsOpenFirstModal(false)}
        title="Модальное окно №1"
        isCloseBtn={true}
        text={text}
        actions={actions}
      />
      <Modal
        isOpen={isOpenSecondModal}
        className="modal"
        closeModalHandler={() => setIsOpenSecondModal(false)}
        title="Модальное окно №2"
        isCloseBtn={true}
        text={text2}
        actions={actions2}
      />
    </div>
  );
}

const StyledButton = styled(Button)`
  font-family: Helvetica, Arial, sans-serif;
  font-size: 15px;
  line-height: 30px;
  color: #ffffff;
  padding: 20px;
  background: ${(props) => props.backgroundColor};
`;


export default App;
