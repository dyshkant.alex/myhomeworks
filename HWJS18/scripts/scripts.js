const IPetrovInfo ={
   name: 'Ivan',
   lastName: 'Petrov',
   family: {
      father :{
         name: 'Petr',
         lastName: 'Petrov',
         birthday: '11.08.1960',
      },
      mother: {
         name: 'Katerina',
         lastName: 'Petrobva',
         birthday: '01.07.1965',
      },
   },
}

function cloneObject(userObject){
   let newObject = {}
   for (let key in userObject){
         if (typeof userObject[key] === "object"){
            newObject[key] = cloneObject(userObject[key])
            } else {
               newObject[key] = userObject[key]
         }
   }
   return newObject
}

let newIPetrov = cloneObject(IPetrovInfo);
delete newIPetrov.family
console.log(IPetrovInfo)
console.log(newIPetrov)