function willDoToDeadLine(membersOfTeam, listOfTasks, deadLineDate){
   const startDate = new Date()
   const storyPointsPerDay = membersOfTeam.reduce((acc, rec) => {return acc + rec})
   const allTasksStoryPoints = listOfTasks.reduce((acc, rec) => {return acc + rec})
   const duration = Math.ceil(allTasksStoryPoints/storyPointsPerDay)
   let qtyDays = 0
   for (let i = duration, day = startDate.getDay(); i > 0;){
      if(day < 6 && day > 0){
         qtyDays += 1
         i -= 1
         day += 1
      }
      else{
         if (day === 0){
            day += 1
            qtyDays += 1
         }
         qtyDays += 2
         day = 1
      }
   }
const endDate  = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() + qtyDays )
 const durationDays = ((deadLineDate - endDate)/(1000*60*60*24)).toFixed()
 if(durationDays >= 0) return `Команда выполнит работу за ${durationDays} до дэдлайна`
 return `Команде нужно ${durationDays * (-1)} после дэдлайна чтобы выполнить работу`
 
}
const deadLineDate = new Date(2020, 04, 29) 
console.log(deadLineDate)
console.log(willDoToDeadLine([5, 5], [5, 25], deadLineDate))