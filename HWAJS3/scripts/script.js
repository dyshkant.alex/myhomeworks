'use strict'
class Employee{
   constructor(name, age, salary){
      this._name = name
      this._age = age
      this._salary = salary
   }
   get name(){
      return this._name
   }
   get age(){
      return this._age
   }
   get salary(){
      return this._salary
   }
   set name(newName){
      this._name = newName
   }
   set age(newAge){
      this._age = newAge
   }
   set salary(newSalary){
      this._salary = newSalary
   }
   
}

 class Programmer extends Employee{
    constructor(name, age, salary, lang){
       super(name, age, salary)
       this.lang = lang

    }
    get salary(){
       return super.salary * 3
    }
 }

 const work = new Employee('Vasya', 25, 1000)
 const prog1 = new Programmer('Viktor', 23, 1000, 'js')
 const prog2 = new Programmer('Ivan', 27, 2000, 'java')
 const prog3 = new Programmer('Oleg', 33, 3000, 'C++')
 console.log(prog1)
 console.log(prog2)
 console.log(prog3)
 console.log(prog1.salary)
