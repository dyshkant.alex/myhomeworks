function findFactorial(number){
   return number == 0 ? 1 : number * findFactorial(number - 1)
   }
let userNumber = prompt('Введите число для которого необходимо найти факториал')
while (userNumber === '' || !Number.isInteger(+userNumber)){
   userNumber = prompt('Введите целое число', userNumber)
}
console.log(findFactorial(userNumber))
