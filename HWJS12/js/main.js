"use stict";
const images = document.querySelectorAll(".image-to-show");
let curent = 0;
let timer;
const delay = 1000;
const stopButton = document.createElement("button");
const continueButton = document.createElement("button");
//const secondsSpan = document.querySelector('#sec')
//const milliSecondsSpan = document.querySelector('#ms')

stopButton.innerHTML = "Stop Animation";
continueButton.innerHTML = "Continue Animation";

document.querySelector(".buttons").append(stopButton);
document.querySelector(".buttons").append(continueButton);

stopButton.addEventListener("click", () => {
  clearInterval(timer);
  timer = 0;
});
continueButton.addEventListener("click", () => {
  if (!timer) {
    if (!curent) slideImg(images);
    else {
      curent -= 1;
      slideImg(images);
    }
  }
});

function slideImg(galery) {
  for (let i = 0; i < galery.length; i += 1) {
    galery[i].classList.remove("showed-now");
  }

  galery[curent].classList.add("showed-now");

  if (curent + 1 === galery.length) {
    curent = 0;
  } else {
    curent += 1;
  }
  timer = setTimeout(slideImg, delay, galery);
}

slideImg(images);

/*let ms = 999
   let sec = (delay/1000) - 1
   const timerDelay = 100
   function startTimer(){     
      secondsSpan.innerHTML = sec
      milliSecondsSpan.innerHTML = ms
      ms -= timerDelay
      if(ms <= 0) {
         ms = 999
         sec -= 1        
      }
      
      timer = setTimeout(startTimer, timerDelay)
      if(sec === 0 ) sec = 9
   } 
*/
