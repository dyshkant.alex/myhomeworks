"use strict";

let newStyle = '<link rel="stylesheet" href="styles/newTheme.css">';
let defaultStyle = '<link rel="stylesheet" href="styles/main.css">';
const head = document.head;
if (
  typeof localStorage["theme"] === undefined ||
  localStorage["theme"] === defaultStyle
) {
  localStorage["theme"] = defaultStyle;
  head.insertAdjacentHTML("beforeend", defaultStyle);
} else head.insertAdjacentHTML("beforeend", localStorage["theme"]);

document.querySelector(".theme").addEventListener("click", function (ev) {
  if (localStorage["theme"] === defaultStyle) localStorage["theme"] = newStyle;
  else localStorage["theme"] = defaultStyle;
  location.reload();
});

$(".page-nav").on("click", "a", function (event) {
  event.preventDefault();
  let id = $(this).attr("href"),
    top = $(id).offset().top;
  $("html").animate({ scrollTop: top }, 1500);
});


const scrollButton = $('#scroll-btn');
$(window).scroll(function () {
  const windowHeight = $(window).height();
  const windowOffsetTop = $(window).scrollTop();

  if (windowOffsetTop > windowHeight) {
    scrollButton.fadeIn(200);
  } else {
    scrollButton.fadeOut(200);
  }
});

scrollButton.click(function () {
  $(document.scrollingElement).animate({ scrollTop: 0 }, 1500);
});


$('#toggle').on('click',  () => {
  if($('#popular-posts').css('display')=== 'none'){
    $('#toggle').text('скрыть')
  }
  else  $('#toggle').text('показать')
$('#popular-posts').slideToggle('slow');
return false
})