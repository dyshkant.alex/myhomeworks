import React, { useEffect} from "react";
import "reset-css";
import {  useDispatch } from "react-redux";

import { Navigation } from "./navigations";

import { getProductsList } from "./store/middleware";


function App() {
  
 const  dispatch = useDispatch()
 
 useEffect(()=>{
  dispatch(getProductsList())  
 },[])  
  
  return (<Navigation />);
}

export default App;
