export const ROUTES = Object.freeze({
   HOMEPAGE: '/',
   CART: '/cart',
   FAVORITE: '/favorite'
})
