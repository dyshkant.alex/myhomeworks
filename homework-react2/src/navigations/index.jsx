import React from "react";
import { Switch, Route } from "react-router-dom";
import { Header } from "../components/Header";

import { FavoritePage } from "../components/pages/FavoritePage";
import { CartPage } from "../components/pages/CartPage";
import { ROUTES } from "./routes";
import { Homepage } from "../components/pages/HomePage";


export const Navigation = () => {
  return (
    <Switch>
      <Route path={ROUTES.CART}>
        <Header />
        <CartPage />
        
      </Route>
      <Route path={ROUTES.FAVORITE}>
        <Header />
        <FavoritePage />
      </Route>
      <Route path={ROUTES.HOMEPAGE}>
        <Header />
        <Homepage />
      </Route>
    </Switch>
  );
};
