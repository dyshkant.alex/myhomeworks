import React from 'react'
import styled from 'styled-components'

export const Error =() =>{
   return(
      <Container>
         <h2>Произошла ошибка загрузки файлов. Перезапустите приложение</h2>
      </Container>
   )
   
}

const Container = styled.div`
   background-image: url("./img/error.jpg");
   display: flex;
   justify-content: center;
   align-items: center;
    h2{
      font-size: 30px;
     
   }

`