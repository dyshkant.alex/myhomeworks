import React from "react";
import styled from "styled-components";
import {  useSelector } from "react-redux";


import {  selectProductsList } from "../../store/product/product-selectors";

import { CardList } from "../productCard/CardList";

export const FavoritePage = () => {
  
  const allProductList = useSelector(selectProductsList)
  const favoriteList = allProductList.filter(({isFavorite}) => isFavorite)
  const isEmpty = favoriteList.length  ? false: true
  return (
    <div>
      {isEmpty && (<Message> Нет товаров в избранном</Message>)}
      <Container>
         <CardList productList={favoriteList} />
      </Container>
    </div>
  );
};

const Container = styled.div`
  max-width: 1200px;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 20px;
  background-color: #e8eaed;
  margin: 0 auto;
  place-content: center;
  padding: 15px;
  box-sizing: border-box;
`;
const Message =styled.p`
  text-align:center;
  font-size: 25px;
  background-color: #e8eaed;
  padding-top: 25px;

`
