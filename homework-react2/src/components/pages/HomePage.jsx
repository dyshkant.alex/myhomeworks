import React from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";

import { ProductCard } from "../productCard/ProductCard";
import { Modal } from "../modals/Modal";
import { Button } from "../buttons/Button";

import {
  selectModalContent,
  selectModalIsOpen,
} from "../../store/modal/modal-selectors";
import { openModal, closeModal } from "../../store/modal/action-creators";
import { addProductToCart } from "../../store/middleware";
import { selectProductsList } from "../../store/product/product-selectors";
import { CardList } from "../productCard/CardList";

export const Homepage = () => {
  const dispatch = useDispatch();
  const modalIsOpen = useSelector(selectModalIsOpen);
  const modalContent = useSelector(selectModalContent);
  const allProductsList = useSelector(selectProductsList);

  const modalButtons = [
    <AddButton
      key="add"
      text="Add to cart"
      onClick={() => {
        dispatch(addProductToCart(modalContent));
        dispatch(closeModal());
      }}
    />,
    <CancelButton key="cancel" text="Cancel" onClick={() => closeModal()} />,
  ];

  function openModalToAdd() {
    const [id] = arguments;
    console.log(id);
    const product = allProductsList.find((it) => it.id === id);
    dispatch(openModal(product));
  }
  const actionButton = {
    text: "Add to Cart",
    handler: openModalToAdd,
  };

  return (
    <div>
      <Container>
        {modalIsOpen && (
          <Modal
            text={<ProductCard {...modalContent} />}
            actions={modalButtons}
            closeModalHandler={() => closeModal()}
            title="Подтвердите Ваше действие"
            id={modalContent.id}
          />
        )}
        <CardList productList={allProductsList} actionButton={actionButton} />
      </Container>
    </div>
  );
};

const Container = styled.div`
  max-width: 1200px;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 20px;
  background-color: #e8eaed;
  margin: 0 auto;
  place-content: center;
  padding: 15px;
  box-sizing: border-box;
`;
const AddButton = styled(Button)`
  border-radius: 4px;
  margin: 10px 5px;
  padding: 5px 10px;
`;
const CancelButton = styled(Button)`
  border-radius: 4px;
  padding: 5px 10px;
  margin: 10px 5px;
  color: red;
`;
