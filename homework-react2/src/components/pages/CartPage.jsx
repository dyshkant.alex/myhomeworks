import React from "react";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";

import { Button } from "../buttons/Button";
import { Modal } from "../modals/Modal";
import { selectProductsList } from "../../store/product/product-selectors";

import {  
  selectModalIsOpen,
  selectModalOptions,
} from "../../store/modal/modal-selectors";
import { openModal, closeModal } from "../../store/modal/action-creators";
import { CardList } from "../productCard/CardList";
import { ProductCard } from "../productCard/ProductCard";
import { CustomerForm } from "../forms/CustomerForm";
import { removeProductToCart } from "../../store/middleware";

export const CartPage = () => {
  const dispatch = useDispatch();

  const allProductList = useSelector(selectProductsList);
  const cartProductsList = allProductList.filter(({ isInCart }) => isInCart);
  //const modalContent = useSelector(selectModalContent);
  const modalIsOpen = useSelector(selectModalIsOpen);
  const modalOptions = useSelector(selectModalOptions);
  const isEmpty = cartProductsList.length ? false : true;

  
  const actionButton = {
    text: "Remove from Cart",
    handler: openModalToRemove,
  };
  
  
  function openModalToRemove() {
    const modalButtons = [
      <RemoveButton
        key="remove"
        text="REMOVE"
        onClick={() => {
          console.log(modalOptions)
          //dispatch(removeProductToCart(modalOptions.text.id));
          //dispatch(closeModal());
        }}
      />,
      <CancelButton key="cancel" text="Cancel" onClick={() => closeModal()} />,
    ];
    const [id] = arguments;
    const product = allProductList.find((it) => it.id === id);
    dispatch(openModal({
      content:<ProductCard {...product}/>,
      actions: modalButtons,
      title: "Подтвердите ваше действие",
      closeModalHandler:()=>dispatch(closeModal())
    }));
  }
  const submitForm =(values) =>{    
  const buyList =   cartProductsList.map(({id, product})=>{
    dispatch(removeProductToCart(id))
     return product})
        
   console.log(values, buyList)
   localStorage.removeItem('cart')

   
   dispatch(closeModal())
  }

  const buyProducts =()=>{
    dispatch(openModal({
      content:<CustomerForm  handleSubmit={submitForm} />,
      actions: [],
      title: "Заполните все поля",
      closeModalHandler:()=>dispatch(closeModal())
    }));
  }
  return (
    <div>
      <Div>
        {isEmpty && <p> Нет товаров в корзине</p>}
        {!isEmpty && <Button onClick={buyProducts} text="Купить товары" />}
      </Div>
      <Container>
        {modalIsOpen && (
          <Modal {...modalOptions} />
        )}
        <CardList productList={cartProductsList} actionButton={actionButton} />
      </Container>
    </div>
  );
};

const Container = styled.div`
  max-width: 1200px;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 20px;
  background-color: #e8eaed;
  margin: 0 auto;
  place-content: center;
  padding: 15px;
  box-sizing: border-box;
`;
const RemoveButton = styled(Button)`
  border-radius: 4px;
  margin: 10px 5px;
  padding: 5px 10px;
  color: red;
`;
const CancelButton = styled(Button)`
  border-radius: 4px;
  padding: 5px 10px;
  margin: 10px 5px;
  color: red;
`;
const Div = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 25px;
  background-color: #e8eaed;
  
    p{
      padding-top: 25px;
    }
    Button{
      margin-top:15px;
      justify-self: end;
    }
`;
