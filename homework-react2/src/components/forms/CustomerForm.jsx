import React from "react";
import { Formik, Field, Form } from "formik";
import * as yup from "yup";
import styled from "styled-components";

import { useDispatch, useSelector } from "react-redux";
import { saveCustomerInfo } from "../../store/customer/action-creators";
import { selectCustomerInfo } from "../../store/customer/customer-selectors";


export const CustomerForm = ({handleSubmit}) => {
 const dispatch = useDispatch()

  const initialValues = useSelector(selectCustomerInfo)

  const handleBlur=(input)=>{
    const key = input.target.name 
    const value = input.target.value
    dispatch(saveCustomerInfo({
      [key]:value
    }))
}
  return (
    <div>
      <Formik
        initialValues={initialValues}
        validationSchema ={ yup.object().shape({
          firstName: yup.string().required(),
          lastName: yup.string().required(),
          age: yup.number().required().positive().integer(),
          address: yup.string().required(),
          phone: yup.string().required(),
        })}
        onSubmit={(values) => {
          handleSubmit(values)        
        }}
        

      >
        {(props) => (
          <StyledForm>
            <Field
              name="firstName"
              type="text"
              placeholder="firstName"
              onBlur={handleBlur}
            />
            {props.errors.firstName ? (
              <ErrorMessage>{props.errors.firstName}</ErrorMessage>
            ) : null}
            <Field
              name="lastName"
              type="text"
              placeholder="lastName"
              onBlur={handleBlur}
            />
            {props.errors.lastName ? (
              <ErrorMessage>{props.errors.lastName}</ErrorMessage>
            ) : null}
            <Field
              name="age"
              type="number"
              placeholder="age"
              onBlur={handleBlur}
            />
            {props.errors.age ? (
              <ErrorMessage>{props.errors.age}</ErrorMessage>
            ) : null}
            <Field
              name="address"
              type="text"
              placeholder="address"
              onBlur={handleBlur}
            />
            {props.errors.address ? (
              <ErrorMessage>{props.errors.address}</ErrorMessage>
            ) : null}
            <Field
              name="phone"
              type="number"
              placeholder="phone"
              onBlur={handleBlur}
            />
            {props.errors.phone ? (
              <ErrorMessage>{props.errors.phone}</ErrorMessage>
            ) : null}
         
            <button type="submit">Buy </button>
          </StyledForm>
        )}
      </Formik>
    </div>
  );
};
const StyledForm = styled(Form)`
  display: flex;
  flex-direction: column;
  width: 60%;
  align-items: center;
  justify-content: center;
  margin: 0 auto;

  input {
    margin: 10px;
    font-size: 18px;
  }
`;
const ErrorMessage = styled.div`
  color: red;
`;
