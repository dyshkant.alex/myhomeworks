import React from "react";
import ReactDOM from "react-dom";

import { Button } from "../buttons/Button";
import styled from "styled-components";
import { useSelector } from "react-redux";
import { selectModalOptions } from "../../store/modal/modal-selectors";

export const Modal = ({ title, closeModalHandler, content, actions, id }) => {
  const modalOptions = useSelector (selectModalOptions);
  console.log(modalOptions)

  return ReactDOM.createPortal(
    <ModalContainer
      onClick={(e) =>
        e.currentTarget === e.target ? closeModalHandler() : null
      }
    >
      <ModalItemsWrapper>
        <ModalHeader>
          <h2>{title}</h2>
          <CloseBtn onClick={closeModalHandler} text="&times;" />
        </ModalHeader>
        <ModalText>{content}</ModalText>
        <ActionButtonContainer>
          {actions.map((it) => {
            return it;
          })}
        </ActionButtonContainer>
      </ModalItemsWrapper>
    </ModalContainer>,
    document.getElementById("modal-root")
  );
};

const ModalContainer = styled.div`
  position: fixed;
  background-color: #312e2e83;
 
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
`;
const ModalItemsWrapper = styled.div`
  background-color: #e8eaed;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-width: 35%;
`;

const ModalHeader = styled.div`
  background-color: #d44637;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-left: 10px;
`;
const ModalText = styled.div`
  display: flex;
  align-self: center;
  justify-content: center;
  padding: 15px 20px;
  font-size: 20px;
  color: #ffffff;
`;

const CloseBtn = styled(Button)`
  font-family: Helvetica, Arial, sans-serif;
  font-size: 15px;
  background-color: inherit;
  color: #ffffff;
  padding: 10px;
  border: none;
`;
const ActionButtonContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-self: center;
`;
