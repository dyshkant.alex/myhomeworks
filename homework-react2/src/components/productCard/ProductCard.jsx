
import React from "react";
import styled from "styled-components";

import { useDispatch} from "react-redux";

import { addProductToFavorites, removeProductFromFavorites } from "../../store/middleware";
import { FavoriteButton } from "../buttons/FavoriteButton";
import { Button } from "../buttons/Button";
export const ProductCard = ({
  id,
  image,
  price,
  product,
  color,
  discount,
  isFavorite,
  actionButton
}) => {

 
  const dispatch = useDispatch()
  
  return (
    <Container>    
      <FavoriteButton isFavorite={isFavorite} delFromFavorites={()=>dispatch(removeProductFromFavorites(id))} addToFavorites={()=>{
        
        return dispatch(addProductToFavorites(id))}} />
      <div>
        <p>{product}</p>
        <ImageContainer>
          <a href={image}>
            <img alt={product} src={image} />
          </a>
        </ImageContainer>
        <Color color={color}>color: {color}</Color>
        <div>
          {discount && <Discount>{(price * 0.9).toFixed(2)} $</Discount>}
          <Price discount={discount}>
            price: <span>{price} $</span>
          </Price>
        </div>
      </div>
     {actionButton&&<Button {...actionButton} onClick={()=>actionButton.handler(id)} />}
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  max-width: 300px;

  flex-direction: column;
  justify-content: space-between;
  background-color: #fff;
  border-radius: 4px;
  padding: 5px;
  color: black;
  p {
    font-size: 20px;
    font-weight: 700;
    margin: 10px 0;
  }
`;

const Discount = styled.span`
  color: #eb0e0e;
  font-size: 30px;
  font-weight: bold;
`;

const Price = styled.p`
  font-size: 20px;
  font-weight: 700;
  margin: 10px 0;
  align-self: flex-end;
  text-decoration: ${(props) => (props.discount ? "line-through" : "")};
`;

const ImageContainer = styled.div`
  height: 150px;
  img {
    height: 100%;
    width: 100%;
    object-fit: cover;
  }
`;
const Color = styled.p`
  color: ${(props) => props.color};
`;
