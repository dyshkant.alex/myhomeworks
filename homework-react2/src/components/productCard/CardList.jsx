import React from 'react'


import { ProductCard } from './ProductCard'


export const CardList = ({productList, actionButton}) =>{
 
 
   return(
      productList.map(({id, ...rest}) =>{
         
        return (<ProductCard
          key={id}
          id={id}
          {...rest}
          actionButton={actionButton} 
          />)
      })

   )
}


