import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

import { ROUTES } from "../navigations/routes";

export const Header = () => {
  return (
    <Container>
      <div>
        <Link path to={ROUTES.HOMEPAGE}>
          HomePage
        </Link>
        <Link path to={ROUTES.CART}>
          Cart
        </Link>
        <Link path to={ROUTES.FAVORITE}>
          Favorite
        </Link>
      </div>
    </Container>
  );
};

const Container = styled.header`
  padding: 10px;
  border-bottom: 1px solid black;
  text-align: center;
  background-color: #062542;
  
 
  div{
   max-width: 1200px;
   margin: 0 auto;
  }

  a {
    color: black;
    text-decoration: none;
    margin: 0 15px;
    text-transform: uppercase;
    font-weight: bold;
    color: #ffffff;
  }
`;
