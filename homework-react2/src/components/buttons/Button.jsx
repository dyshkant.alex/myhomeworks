import React from 'react'
import styled from 'styled-components'


export const Button = ({text, onClick, className, title=''}) => {
  
   return(
      <StyledBtn title={title} className={className} onClick={onClick}>
         {text}
      </StyledBtn>
   )
}

const StyledBtn = styled.button`
   outline:none;
`
