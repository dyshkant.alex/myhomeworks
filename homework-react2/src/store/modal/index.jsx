
import { CLOSE_MODAL, OPEN_MODAL } from "./actions-types";






const initialState = {    
  
  isOpenModal: false,
  options:{
}
};

export function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case OPEN_MODAL:
      return {
        ...state,        
        options:payload,
        isOpenModal: true,
       
      };
    case CLOSE_MODAL:
      return {
        ...state,               
       isOpenModal: false,
      };
    default:
      return state;
  }
}

