//Module name
export const MODULE_NAME = "modal";
//selectors
export const selectModalOptions = state=> state[MODULE_NAME].options;
export const selectModalIsOpen = state => state[MODULE_NAME].isOpenModal;
export const selectModalContent = state=> state[MODULE_NAME].content;