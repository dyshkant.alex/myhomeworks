import {createStore, combineReducers, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';

import {reducer as productsReducer} from './product';
import { MODULE_NAME as productsModuleName} from './product/product-selectors';
import { MODULE_NAME as modalModuleName } from './modal/modal-selectors';
import{reducer as modalReducer} from "./modal/index"
import { MODULE_NAME as customerModuleName } from './customer/customer-selectors';
import{reducer as customerReducer} from "./customer/index"




const rootReducer = combineReducers({
   [productsModuleName]: productsReducer,
   [modalModuleName]:modalReducer,
   [customerModuleName]:customerReducer
})


export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))
