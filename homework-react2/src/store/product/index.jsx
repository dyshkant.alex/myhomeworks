import { ADD_TO_CART, ADD_TO_FAVORITES, REMOVE_FROM_CART, REMOVE_FROM_FAVORITES,   SET_PRODUCTS } from "./actions-types";



const initialState = {
 productsList: [],

   
};
export function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_PRODUCTS:
      return {
        ...state,
        productsList: payload,       
      };
   
    case ADD_TO_CART:
      return {
        ...state,
        productsList:state.productsList.map(product => {
          if (product.id === payload) {
            product.isInCart = true
          }
          return product})
      };
    case REMOVE_FROM_CART:
      return {
        ...state,
        productsList:state.productsList.map(product => {
          if (product.id === payload) {
            product.isInCart = false
          }
          return product})
      };
    
    case ADD_TO_FAVORITES:
      return {
        ...state,
        productsList:state.productsList.map(product => {
          if (product.id === payload) {
            product.isFavorite = true
          }
          return product})
           
     ,
      };
    case REMOVE_FROM_FAVORITES:
      return {
        ...state,
        productsList:state.productsList.map(product => {
          if (product.id === payload) {
            product.isFavorite = false
          }
          return product})
          
      };
    default:
      return state;
  }
}



