//Module name
export const MODULE_NAME = "products";
//selectors
export const selectProductsList = (state) => state[MODULE_NAME].productsList;
export const selectСartProductsList = (state) =>
state[MODULE_NAME].cartProductsList;
export const selectFavoriteProductsList = (state) =>
state[MODULE_NAME].favoriteProductsList;
