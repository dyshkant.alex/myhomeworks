import { ADD_TO_CART, ADD_TO_FAVORITES, REMOVE_FROM_CART, REMOVE_FROM_FAVORITES,   SET_PRODUCTS } from "./actions-types";



export const setProducts = (payload) => ({
   type: SET_PRODUCTS,
   payload,
   });
   
  
   export const addToCart = (payload) => ({
   type: ADD_TO_CART,
   payload,
   });
   
   export const removeFromCart = (payload) => ({
   type: REMOVE_FROM_CART,
   payload,
   });
   
   ;
   export const addToFavorites = (payload) => ({
   type: ADD_TO_FAVORITES,
   payload,
   });
   
   export const removeFromFavorites = (payload) => ({
   type: REMOVE_FROM_FAVORITES,
   payload,
   });
