import  {BASE_URL} from "../API";
import {
  addToCart,
  removeFromCart,
   addToFavorites,
   removeFromFavorites,
 
  setProducts,
} from "./product/action-creators";

export const getProductsList = () => async (dispatch) => {
  try {
    
    const { status, data } = await BASE_URL.get();
    if (status === 200) {
      const productsList = Object.keys(data).map((key) => ({
        id: key,
        isFavorite: JSON.parse(localStorage.getItem("favorites"))
          ? JSON.parse(localStorage.getItem("favorites")).includes(key)
          : false,
        isInCart: JSON.parse(localStorage.getItem("cart"))
          ? JSON.parse(localStorage.getItem("cart")).includes(key)
          : false,
        ...data[key],
      }));
      dispatch(setProducts(productsList));
    }
  } catch (error) {
    console.log(error);
  }
};
//-------------CART---------------------------------------------

export const addProductToCart = ({id}) => (dispatch) => {
  const cart = new Set(JSON.parse(localStorage.getItem("cart"))) || new Set();
  cart.add(id);
  dispatch(addToCart(id))
  localStorage.setItem("cart", JSON.stringify([...cart]));
  

};
export const removeProductToCart = (id) => (dispatch) => {
const cart =
   new Set(JSON.parse(localStorage.getItem("cart"))) || new Set();
cart.delete(id);
dispatch(removeFromCart(id))
localStorage.setItem("cart", JSON.stringify([...cart]));
}

//-------------Favorites---------------------------------------------
export const removeProductFromFavorites = (id) =>(dispatch)=> {
  const favorites =
    new Set(JSON.parse(localStorage.getItem("favorites"))) || new Set();
  favorites.delete(id);
  localStorage.setItem("favorites", JSON.stringify([...favorites]));
  dispatch(removeFromFavorites(id));
};
export const addProductToFavorites = (id) =>(dispatch)=> {
   const favorites =
      new Set(JSON.parse(localStorage.getItem("favorites"))) || new Set();   
   favorites.add(id);
   dispatch(addToFavorites(id))
   localStorage.setItem("favorites", JSON.stringify([...favorites]));
      
};
