import { SAVE_CUSTOMER_INFO } from "./action-types";

const initialState = {
  firstName: "",
  lastName: "",
  age: "",
  address: "",
  phone: "",
};

export function reducer(state = initialState, { type, payload }) {
  switch (type) {
   case SAVE_CUSTOMER_INFO:
      return{
         ...state,
          ...payload      


      }

   default:
      return state;
  }
}
