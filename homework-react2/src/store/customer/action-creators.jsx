import { SAVE_CUSTOMER_INFO } from "./action-types";

export const saveCustomerInfo = (payload) => ({
   type: SAVE_CUSTOMER_INFO,
   payload,
   });