const icons = document.querySelectorAll('.icon-password')
const submitButton = document.querySelector('.btn[type="submit"]')

console.log(submitButton)

for (const icon of icons){
   icon.addEventListener('click', showHidePassword)
}

submitButton.addEventListener('click', (ev) => {
   if(document.getElementById('password').value === document.getElementById('confirm').value){
      alert('You are welcome')
   }
   else{

      document.getElementById('confirm').closest('label').insertAdjacentHTML('afterend', '<span style="color:red">Нужно ввести одинаковые значения</span>')
   }
   ev.preventDefault()
})

function showHidePassword(ev){
   const input = ev.target.closest('label').querySelector('input')
   
   let icon =  ev.target
   if (icon.classList.contains('fa-eye-slash')){
      icon.classList.remove('fa-eye-slash')
      icon.classList.add('fa-eye')
      input.type = 'text'
   }
   else{
      icon.classList.remove('fa-eye')
      icon.classList.add('fa-eye-slash')
      input.type = 'password'
   }
  
}