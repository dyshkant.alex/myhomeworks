'use strict'

const tabs = document.querySelector('.tabs')
const tabsContent =  document.querySelector('.tabs-content')
console.log(tabs)
tabs.addEventListener('click', showContent)

function showContent(ev){
  
   const dataID = ev.target.dataset.id
   console.log(dataID)
   tabs.querySelector('.active').classList.remove('active')
   ev.target.classList.add('active')
   tabsContent.querySelector('.active').classList.remove('active')
   tabsContent.querySelector(`#${dataID}`).classList.add('active')   
}