//============================задание 1============================
const [city1, city2, city3, city4, city5] = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"]
 console.log(city1, city2, city3, city4, city5)

//============================задание 2============================
   
const Employee = {
   name: 'Jon',
   salary: 5000,
}

const {name, salary} = Employee
 console.log(name, salary)
 
//============================задание 3============================
array = ['value', 'showValue']
const {0:value, 1: showValue} = array
 alert(value)
 alert(showValue)
