const priceInput = document.createElement('input')
priceInput.classList.add('price')
const closeBtn = document.createElement('button')
const infoSpan = document.createElement('span')
const priceContainer =document.querySelector('.container')
const errorSpan = document.createElement('span')
errorSpan.innerHTML ='<b style="color: red;"> Please enter correct price</b>'


function createSpan(input){     
   infoSpan.innerHTML = `Текущая цена: <span class='curent-price'> ${input.value} </span><button class='close-btn'>&times</button>`
   priceContainer.prepend(infoSpan)   
   infoSpan.style.cssText = `    
   border: 1px solid black;
   display: flex;
   justify-content: space-between; 
   align-content: center;  
   border-radius: 10px ;
   padding: 0 10px;
   display: block; 
   margin: 1px 0  `  
  
}

priceContainer.append(priceInput)


priceInput.addEventListener('focus', (ev)=>{
   ev.target.style.borderColor = 'green'
})

priceInput.addEventListener('blur', (ev)=>{  
   ev.target.style.borderColor = 'red'  
})

priceInput.addEventListener('change', function onChange(ev){
   if (this.value < 0){
      infoSpan.remove()
      this.style.borderColor = 'red'
      this.insertAdjacentElement("afterend", errorSpan)
   } 
   else  {
      errorSpan.remove()
      createSpan(ev.target)
      this.select()
    
      infoSpan.querySelector('.close-btn').addEventListener('click', () => {
         infoSpan.remove()
         this.value = ''       
   })
}})