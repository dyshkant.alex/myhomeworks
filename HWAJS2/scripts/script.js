'use strict'

const books = [
   {
   author: "Скотт Бэккер",
   name: "Тьма, что приходит прежде",
   price: 70
   },
   {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
   },
   {
   name: "Тысячекратная мысль",
   price: 70
   },
   {
   author: "Скотт Бэккер",
   name: "Нечестивый Консульт",
   price: 70
   },
   {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
   },
   {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
   }
   ];
   const ul = document.createElement('ul')
   document.getElementById('root').append(ul)
   
   function renderList(obj){
      let {name, price, author} = obj      
      let li = `<li>${name}<ul><li> Автор: ${author} <li>Цена: ${price}</li></li></ul></li>`
      try {
         if(!name || !price || !author)  throw new Error('Неполные данные')
       }
       catch (er){
          if(er) {
             console.error(`${er.message}`)
             return
          }
       }         
     
      ul.innerHTML += li
   }

books.forEach( el => renderList(el))