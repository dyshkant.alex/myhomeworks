"use strict";
const buttonIp = document.getElementById("findIP");

function getInformationIp() {
  fetch("http://api.ipify.org/?format=json")
    .then((data) => {
      if (!data.ok) throw new Error(data.statusText);
      return data.json();
    })
    .then((data) => {
      console.log(data.ip);
      return fetch(`http://ip-api.com/json/${data.ip}?fields=40987`);
    })
    .then((data) => data.json())
    .then((data) => {
      const ul = document.createElement('ul')
      for (let [key, value] of Object.entries(data)) {
       ul.innerHTML += `<li>${key}: ${value}</li>`
      }
      buttonIp.after(ul)
    });
}

buttonIp.addEventListener("click", getInformationIp);
