'use strict'
const userArray  = [
   "hello",
   "world",
   ["Kyiv", "city", ["forest", "lake"]],
   "Kharkiv",
   "Odessa",
   "Lviv"
   ]

 createList(userArray)

 function createList(array) {
   const ul = document.createElement("ul");
   const content = createContent(array);
   ul.innerHTML = content;
   document.body.append(ul);
   timer(10)

  
   function createContent(array) {
      return array.map(el => {
         if (typeof el !== 'object'&& el !== null) return `<li>${el}</li>`;
         else return `<li><ul>${createContent(el)}</ul></li>`;
      }).join('');     
      
      }
   

   function timer (seconds) {  
      let secondsSpan = document.querySelector('.seconds')
      secondsSpan.textContent = 'seconds' 
      if (seconds > 0) {   
         secondsSpan.textContent = seconds
        
         seconds -= 1;
         setTimeout(function () {
            timer(seconds);
         }, 1000);
      } else {
      document.body.hidden = true
      }
   } 
 }


